output "public_security_group_id" {
    value = aws_security_group.airflow_publicsg.id
}

output "private_security_group_id" {
    value = aws_security_group.airflow_privatesg.id
}


